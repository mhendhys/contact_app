import React, { useState, useEffect } from "react";
import { connect } from 'react-redux';
import { fetchContacts, fetchContact, deleteContact, updateContact } from '../redux/action/action';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  ImageBackground,
  Pressable,
  ActivityIndicator,
  TouchableOpacity
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { DimHeight, DimWidth } from "../helpers/size";

export const ContactDetail =(props: any) => {

  const { contactDetail, route, navigation } = props;

  useEffect(() => {
    getEntities();
  }, []);

  const getEntities = () => {
    props.fetchContact(route.params.id);
  };

  const image = {uri: 'https://i.pinimg.com/736x/37/61/bf/3761bf6bcd560bc170873908030e2e9a.jpg'};
  return (
    <View style={styles.container}>
      {
        Object.keys(contactDetail).length > 0 ? 
        <>
          <View>
          {
            contactDetail.photo.startsWith("file") ? 
            <View>
              <ImageBackground
              source={{ uri: contactDetail ? contactDetail.photo : "" }}
              style={{ ...styles.backgroundImage }}
              >

                <View style={styles.buttonEdit}>
                  <TouchableOpacity style={{ marginHorizontal: 20}}
                  >
                    <FontAwesome
                      name="star"
                      size={20}
                      color="black"
                      />
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => {navigation.navigate('ContactUpdate', { id: contactDetail.id})}}
                  >
                    <FontAwesome
                    name="pencil"
                    size={20}
                    color="black"
                    />
                  </TouchableOpacity>
                </View>
              </ImageBackground>
            </View>
            :
            <View style={{...styles.backgroundImage, backgroundColor: 'orange'}}>          
            <View style={styles.buttonEdit}>
                  <TouchableOpacity style={{ marginHorizontal: 20}}
                  >
                    <FontAwesome
                      name="star"
                      size={20}
                      color="white"
                      />
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => {navigation.navigate('ContactUpdate', { id: contactDetail.id})}}
                  >
                    <FontAwesome
                    name="pencil"
                    size={20}
                    color="white"
                    />
                  </TouchableOpacity>
                </View>
            </View>
          }
          </View>
          <ImageBackground 
          source={image}
          imageStyle={styles.card}
          style={styles.card}>
            <View style={{ top: DimHeight(3), }}>
              <View style={{ flexDirection: 'row', justifyContent: 'center'}}>
                <Text style={styles.mainText}>{contactDetail.firstName + " " + contactDetail.lastName}</Text>
              </View>
              <View style={{ flexDirection: 'row', justifyContent: 'center'}}>
                <Text style={{ fontSize: 18, color: 'grey'}}>mobile</Text>
                <Text style={styles.mainPhone}>+62821-1221-2112</Text>
              </View>
              <View style={{ flexDirection: 'row', justifyContent: 'center'}}>
                <Text style={{ fontSize: 18, color: 'grey', alignSelf: 'center'}}>age</Text> 
                <Text style={styles.mainAge}>{contactDetail.age}</Text>
              </View>
              <View style={styles.sim}>
                <FontAwesome5
                  name="sim-card"
                  size={20}
                  color="#068FFF"
                />
                <Text style={{ fontSize: 16, color: 'black'}}>Call with default : SIM 1</Text> 
              </View>
              <View style={{ flexDirection: 'row', justifyContent: 'center'}}>
                <FontAwesome5
                  style={styles.iconCall}
                  name="phone"
                  size={25}
                  color="orange"
                />
                <FontAwesome5
                  style={styles.iconCall}
                  name="video"
                  size={25}
                  color="orange"
                />
                <FontAwesome5
                  style={styles.iconCall}
                  name="sms"
                  size={25}
                  color="orange"
                />
              </View>
            </View>
          </ImageBackground>
        </>
        :
        <View style={{ flex: 1, justifyContent: 'center'}}>
          <ActivityIndicator size="large" />
        </View>
      }
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  card: {
    flex: 1,
    backgroundColor: 'white',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    marginTop : DimHeight(-2),
    elevation: 9,
    shadowColor: '#014c75',
  }, 
  backgroundImage: {
    width: Dimensions.get('screen').width,
    height: Dimensions.get('screen').height/2,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0,0,0,0.5)'
  },
  mainText:{
    paddingLeft: DimWidth(3),
    fontSize: DimWidth(9),
    color: '#262626',
    paddingVertical: DimHeight(1)
  },
  mainAge:{
    fontSize: DimWidth(6),
    color: '#262626',
    paddingLeft: DimWidth(2)
  },
  mainPhone:{
    fontSize: DimWidth(6),
    color: '#262626',
    paddingLeft: DimWidth(3)
  },
  mainIcon:{
    alignSelf: 'center',
    left: DimWidth(5),
    paddingRight: DimWidth(9),
  },
  iconCall:{
    alignSelf: 'center',
    paddingHorizontal: DimWidth(9),
    paddingVertical: DimWidth(9),
  },
  buttonEdit:{
    alignSelf: 'flex-end',
    bottom: DimHeight(6),
    right:DimWidth(7),
    flexDirection: 'row'
  },
  buttonCall:{
    position: 'absolute',
    top: DimHeight(1),
    right:DimWidth(7),
    display: 'flex',
    flexDirection: 'row'
  },
  sim : { 
    flexDirection: 'row', 
    justifyContent: 'space-evenly',
    alignSelf: 'center',
    backgroundColor: '#E2DFD2',
    width: DimHeight(30),
    marginTop: DimHeight(3),
    paddingVertical: DimHeight(1),
    borderRadius: 30

  }
})

const mapStateToProps = ({ contactReducer } : any) => ({
  contactDetail: contactReducer.contact
});

const mapDispatchToProps = {
  fetchContacts,
  fetchContact,
  deleteContact,
  updateContact
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ContactDetail);